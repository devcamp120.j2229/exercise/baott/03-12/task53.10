package com.devcamp.j04_javabasic.s10;
public class CPet extends CAnimal {
    protected int age;
    protected String name;

    @Override
    public void animalSound() {
        System.out.println("Pet sound...");
    }

    protected void print() {

    }
    
    protected void play() {
        
    }
}
