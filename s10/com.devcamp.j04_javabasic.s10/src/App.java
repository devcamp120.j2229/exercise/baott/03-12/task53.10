import java.util.ArrayList;

import com.devcamp.j04_javabasic.s10.CAnimal;
import com.devcamp.j04_javabasic.s10.CFish;
import com.devcamp.j04_javabasic.s10.CPerson;
import com.devcamp.j04_javabasic.s10.CPet;


public class App {
    public static void main(String[] args) throws Exception {
        // CPet myPet = new CPet();
        // myPet.name = "KIYA";
        // myPet.age = 2;
        // myPet.eat();
        // myPet.animalSound();
        // myPet.print();
        // myPet.play();
        // myPet.animclass = AnimalClass.mammals;

        // CPet fish = new CFish();
        // fish.name = "Gold Fish";
        // fish.animclass = AnimalClass.fish;
        // fish.eat();
        // fish.animalSound();
        // fish.print();
        // fish.play();
        // ((CFish) fish).swim();
        // ep kieu du lieu | casting

        // System.out.println("##########################################");
        // System.out.println("##########################################");

        // CBird bird = new CBird();
        // bird.name = "My Eagle";
        // bird.animclass = AnimalClass.birds;
        // bird.eat();
        // bird.animalSound();
        // bird.print();
        // bird.play();
        // bird.fly();

        CAnimal nameA2 = new CFish();
        CPet name2 = new CPet();
        CPerson person = new CPerson();
        person.setId(123456);
        person.setAge(25);
        person.setFirstName("John");
        person.setLastName("Doe");

        // add cac pet vao array
        ArrayList<CPet> pets = new ArrayList<>(); // javascript : ["asda", "sdas"]
        pets.add((CPet) nameA2);
        pets.add(name2);

        // set pets cho Person
        person.setPets(pets);   

        // print ra console
        System.out.println(person);

    }
}
